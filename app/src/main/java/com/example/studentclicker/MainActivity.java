package com.example.studentclicker;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

/**
 * Класс главной активити приложения "Кликер"
 * @author Chelnokov E.I.
 * @author ita
 */

public class MainActivity extends AppCompatActivity {
    private Integer counter = 0;//счётчик студентов
    private Integer level = 1;//счётчик уровня

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        updateLevel();
        updateCounter();
    }

    public void onClickBtnAddStudents(View view) {
        counter = counter + 5000;
        if (counter == 1_000_000){
            level++;
            counter = 0;
        }
        updateCounter();
        updateLevel();
    }

    /**
     * сохраняет значения счётчиков
     * @param outState объект с текущими позиицями элементов
     */
    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        outState.putInt("txt_counter", counter);// пара "ключ-значение"
        outState.putInt("txt_level", level);
        super.onSaveInstanceState(outState);
    }

    /**
     * восстанавливает значения счётчиков
     * @param savedInstanceState набор сохранённых значений
     */
    @Override
    protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        counter = savedInstanceState.getInt("txt_counter");//приём данных по ключу
        level = savedInstanceState.getInt("txt_level");
        super.onRestoreInstanceState(savedInstanceState);
        updateCounter();
        updateLevel();
    }

    /**
     *обновляет значение счётчика студентов на экране
     */
    private void updateLevel() {
        TextView levelView = findViewById(R.id.level);
        levelView.setText(getString(R.string.level) + level);
    }

    /**
     * обновляет значение уровня на экране
     */
    private void updateCounter() {
        TextView counterView = (TextView) findViewById(R.id.txt_counter);
        counterView.setText(counter.toString());
    }
}